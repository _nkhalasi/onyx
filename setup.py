from setuptools import setup, find_packages
setup(
    name='onyx',
    version='1.0',
    description='Product price list application',
    author='Naresh Khalasi',
    author_email='naresh.khalasi@gmail.com',
    packages=find_packages(),
    install_requires=(
        'flask',
        'flask-login',
        'sqlalchemy == 1.0.2',
        'wtforms',
        'mysql-connector-python',
    ),
    dependency_links = (),
)
