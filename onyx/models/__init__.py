import decimal
from itertools import groupby
#from flask.ext.login import unicode
from sqlalchemy import types, Column, Unicode, func, CHAR, UniqueConstraint, ForeignKey, Enum
from sqlalchemy.dialects import mysql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker, relationship

__all__ = ('Session',)

Session = None


def get_db_session(engine=None):
    global Session
    if Session is None:
        Session = scoped_session(sessionmaker())
        Session.configure(bind=engine)
    return Session


class _Base(object):
    """Class used as the root for declarative base of SQLAlchemy objects"""
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }

    def __repr__(self):
        L = []
        for k in self.__class__.__table__.c.keys():
            value = getattr(self, k, '')
            L.append("%s=%r" % (k, value))
        return '%s(%s)' % (self.__class__.__name__, ','.join(L))

    @classmethod
    def find_by_id(cls, context, id_):
        return context.db_session.query(cls).filter(cls.id == id_).first()


Base = declarative_base(cls=_Base)
bigint = mysql.BIGINT(unsigned=True)
text_type = mysql.TEXT(unicode=True)
signed_bigint = mysql.BIGINT(unsigned=False)
amount_type = types.Numeric(20, 4)


class Role(object):
    USER = 'user'
    ADMIN = 'admin'


class Principal(Base):
    __tablename__ = 'principals'

    id = Column(bigint, primary_key=True, autoincrement=True)
    handle = Column(CHAR(16), nullable=False, unique=True)
    name = Column(Unicode(50), nullable=False)
    email = Column(Unicode(80), nullable=True)
    role = Column(Unicode(20), nullable=False, default=Role.USER)
    enabled = Column(types.BOOLEAN, nullable=False, default=True)
    principal_type = Column(types.Unicode(30), nullable=False)
    credentials = Column(Unicode(200), nullable=False)
    created_on = Column(types.DATETIME, nullable=False, default=func.current_timestamp())
    last_updated = Column(types.DATETIME, nullable=True, onupdate=func.current_timestamp())
    version = Column(types.SMALLINT, nullable=False, default=0)

    __mapper_args__ = {'polymorphic_on': principal_type, 'polymorphic_identity':'user', 'version_id_col': version}


class User(Principal):
    __mapper_args__ = {'polymorphic_identity': 'user'}

    def is_authenticated(self):
        return True

    def is_active(self):
        return self.enabled

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)


class Product(Base):
    __tablename__ = 'products'

    id = Column(bigint, primary_key=True, autoincrement=True)
    code = Column(Unicode(30), nullable=False, unique=True)
    desc = Column(Unicode(250), nullable=False)
    category = Column(Unicode(250), nullable=False)
    created_on = Column(types.DATETIME, nullable=False, default=func.current_timestamp())
    last_updated = Column(types.DATETIME, nullable=True, onupdate=func.current_timestamp())
    version = Column(types.SMALLINT, nullable=False, default=0)

    component_defs = relationship(lambda: ProductComponent, order_by=lambda: ProductComponent.component_index)

    __mapper_args__ = {'version_id_col': version}

    def __init__(self, **kwargs):
        self.category = kwargs.get('category', kwargs['desc'])
        self.code = kwargs.get('code', None)
        self.desc = kwargs.get('desc', None)

    @property
    def unique_component_defs_by_index(self):
        seen = set()
        seen_add = seen.add
        return tuple(x for x in self.component_defs if x.component_index not in seen and not seen_add(x.component_index))

    @property
    def component_defs_by_index(self):
        return tuple(
            (idx, tuple(pcdef.component_def for pcdef in pcdefs))
            for idx, pcdefs in groupby(self.component_defs, key=lambda cd: cd.component_index)
        )


price_depends_on_enum = Enum('c2cd', 'moc')


class ComponentDef(Base):
    __tablename__ = 'component_definitions'

    id = Column(bigint, primary_key=True, autoincrement=True)
    name = Column(Unicode(200), nullable=False, unique=True)
    specs = Column(Unicode(1000), nullable=True)
    price_depends_on = Column(price_depends_on_enum, nullable=True)
    created_on = Column(types.DATETIME, nullable=False, default=func.current_timestamp())
    last_updated = Column(types.DATETIME, nullable=True, onupdate=func.current_timestamp())
    version = Column(types.SMALLINT, nullable=False, default=0)

    __mapper_args__ = {'version_id_col': version}

    @property
    def is_center_to_center_distance(self):
        return self.name.lower() == 'center to center distance'


class ProductComponent(Base):
    __tablename__ = 'product_compositions'

    product_id = Column(bigint, ForeignKey('products.id', name='fk_pc_product'), primary_key=True)
    component_def_id = Column(bigint, ForeignKey('component_definitions.id', name='fk_pc_component'), primary_key=True)
    component_index = Column(types.SMALLINT, nullable=False, autoincrement=False, primary_key=True)
    component_def = relationship(ComponentDef)
    version = Column(types.SMALLINT, nullable=False, default=0)

    __mapper_args__ = {'version_id_col': version}


class Component(Base):
    __tablename__ = 'components'

    id = Column(bigint, primary_key=True, autoincrement=True)
    def_id = Column(bigint, ForeignKey('component_definitions.id'), nullable=False)
    code = Column(Unicode(30), nullable=False)
    desc = Column(Unicode(250), nullable=False)
    moc = Column(Unicode(30), nullable=True)
    base_price = Column(types.Numeric(20,4), nullable=False, default=decimal.Decimal("0.0000"))
    created_on = Column(types.DATETIME, nullable=False, default=func.current_timestamp())
    last_updated = Column(types.DATETIME, nullable=True, onupdate=func.current_timestamp())
    version = Column(types.SMALLINT, nullable=False, default=0)

    component_def = relationship(ComponentDef, backref='components')

    __table_args__ = (UniqueConstraint(def_id, code, moc, name='uq_component_code_moc'), Base.__table_args__)
    __mapper_args__ = {'version_id_col': version}

    @property
    def is_center_to_center_distance(self):
        return self.code.lower() == 'c2cd'

    @property
    def fixed_priced(self):
        return self.component_def.price_depends_on is None

    @property
    def price_depends_on(self):
        return self.component_def.price_depends_on


class ComponentDetail(Base):
    __tablename__ = 'component_details'

    id = Column(bigint, primary_key=True, autoincrement=True)
    component_id = Column(bigint, ForeignKey('components.id'), nullable=False)
    name = Column(Unicode(30), nullable=False)
    value = Column(Unicode(50), nullable=False)
    index = Column(types.SMALLINT, nullable=False, default=0)
    version = Column(types.SMALLINT, nullable=False, default=0)

    component = relationship(Component, backref='details')

    __mapper_args__ = {'version_id_col': version}


upload_status_enum = Enum('received', 'processing', 'processed', 'processing-failed')


class UploadJob(Base):
    __tablename__ = 'upload_jobs'

    id = Column(bigint, primary_key=True, autoincrement=True)
    upload_time = Column(types.DateTime, nullable=False, default=func.current_timestamp())
    upload_type = Column(types.Unicode(50), nullable=False)
    upload_filename = Column(types.Unicode(255), nullable=False)
    storage_filename = Column(types.Unicode(255), nullable=False)
    upload_user_id = Column(bigint, ForeignKey('principals.id'), nullable=False)
    upload_user = relationship(Principal)
    upload_status = Column(upload_status_enum, nullable=False)
    created_on = Column(types.DATETIME, nullable=False, default=func.current_timestamp())
    last_updated = Column(types.DATETIME, nullable=True, onupdate=func.current_timestamp())
    version = Column(types.SMALLINT, nullable=False, default=0)

    __mapper_args__ = {'version_id_col': version}
