class ComponentMeta(object):
    def __init__(self, name, specs):
        self.name = name
        self.specs = specs


class _Component(object):
    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)
        self.priced = False

    def price(self):
        return 0.0

    def __repr__(self):
        return '<{} - {}::{}::{}>'.format(self.__class__.__name__, self.code, self.desc, self.price())


class _PricedComponent(_Component):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.priced = True

    def price(self):
        return self.base_price


class ProcessConnectionOrientation(_Component):
    pass

process_connection_orientations = (
    ProcessConnectionOrientation(code='TBV', desc='Top-Bottom Vertical(Partial Visibility)'),
    ProcessConnectionOrientation(code='SSR', desc='Side-Side Right(Full Visibility)'),
    ProcessConnectionOrientation(code='SSL', desc='Side-Side Left(Full Visibility)'),
    ProcessConnectionOrientation(code='SSB', desc='Side-Side Back(Full Visibility)'),
)


class CenterToCenterDistance(_PricedComponent):
    def price(self):
        return float(self.code) * self.base_price

c2c_distance = (
    CenterToCenterDistance(code='1000', desc='Center to center distance in mm', base_price=10.35),
)


class _ProcessConnection(_PricedComponent):
    pass


class FlangedProcessConnection(_ProcessConnection):
    pass


class ScrewedProcessConnection(_ProcessConnection):
    pass

process_connections = (
    FlangedProcessConnection(code='15F150', code1='15', code2='F', code3='150', desc='Flanged Connection 15F150', size1='0.50 in', size2='150#RF', base_price=5.75),
    FlangedProcessConnection(code='20F300', code1='20', code2='F', code3='300', desc='Flanged Connection 20F300', size1='0.75 in', size2='300#RF', base_price=5.75),
    FlangedProcessConnection(code='25F600', code1='25', code2='F', code3='600', desc='Flanged Connection 25F600', size1='1.00 in', size2='600#RF', base_price=5.75),
    FlangedProcessConnection(code='40F900', code1='40', code2='F', code3='900', desc='Flanged Connection 40F900', size1='1.50 in', size2='900#RF', base_price=5.75),
    FlangedProcessConnection(code='50F1500', code1='50', code2='F', code3='1500', desc='Flanged Connection 50F1500', size1='2.00 in', size2='1500#RF', base_price=5.75),
    ScrewedProcessConnection(code='??SNPTM', code1='0.50in', code2='S', size='0.50 in', desc='Screwed Connection ??SNPTM', threading_type='NPTM', base_price=5.75),
    ScrewedProcessConnection(code='??SBSPM', code1='0.75in', code2='S', size='0.75 in', desc='Screwed Connection ??SBSPM', threading_type='BSPM', base_price=5.75),
    ScrewedProcessConnection(code='??SNPTF', code1='1.00in', code2='S', size='1.00 in', desc='Screwed Connection ??SNPTF', threading_type='NPTF', base_price=5.75),
    ScrewedProcessConnection(code='??SBSPF', code1='1.50in', code2='S', size='1.50 in', desc='Screwed Connection ??SBSPF', threading_type='BSPF', base_price=5.75),
)


class LiquidChamberMOC(_PricedComponent):
    pass

liquid_chamber_mocs = (
    LiquidChamberMOC(code='C', desc='CS', base_price=6.10), LiquidChamberMOC(code='S4', desc='SS 304', base_price=6.10),
    LiquidChamberMOC(code='S4L', desc='SS 304L', base_price=6.10), LiquidChamberMOC(code='S6', desc='SS 316', base_price=6.10),
    LiquidChamberMOC(code='S6L', desc='SS 316L', base_price=6.10), LiquidChamberMOC(code='PP', desc='Polypropylene', base_price=6.10),
    LiquidChamberMOC(code='M', desc='Monel', base_price=6.10), LiquidChamberMOC(code='T', desc='Titanium', base_price=6.10),
    LiquidChamberMOC(code='I', desc='Inconel 600', base_price=6.10), LiquidChamberMOC(code='H', desc="Hastolley 'C'", base_price=6.10),
)


class CoverPlateMOC(_PricedComponent):
    pass

cover_plate_mocs = (
    CoverPlateMOC(code='C', desc='CS', base_price=4.23), CoverPlateMOC(code='S4', desc='SS 304', base_price=4.23),
    CoverPlateMOC(code='S4L', desc='SS 304L', base_price=4.23), CoverPlateMOC(code='S6', desc='SS 316', base_price=4.23),
    CoverPlateMOC(code='S6L', desc='SS 316L', base_price=4.23), CoverPlateMOC(code='PP', desc='Polypropylene', base_price=4.23),
    CoverPlateMOC(code='M', desc='Monel', base_price=4.23), CoverPlateMOC(code='T', desc='Titanium', base_price=4.23),
    CoverPlateMOC(code='I', desc='Inconel 600', base_price=4.23), CoverPlateMOC(code='H', desc="Hastolley 'C'", base_price=4.23),
)


class Fastener(_PricedComponent):
    pass

fasteners = (
    Fastener(code='C', desc='ASTM A 193 Gr. B7 / ASTM A 194 Gr. 2H', base_price=4.17),
    Fastener(code='SS', desc='SS', base_price=4.17),
)


class ToughenedBorosilicateGlass(_PricedComponent):
    pass

tb_glass = (
    ToughenedBorosilicateGlass(code='L', desc='Indigenous', base_price=5.50),
    ToughenedBorosilicateGlass(code='F', desc='Klinger/Illmadur make or equivalent', base_price=5.50),
)


class Cushion(_PricedComponent):
    pass

cushions = (
    Cushion(code='C', desc='C.A.F', base_price=6.34),
    Cushion(code='P', desc='P.T.F.E', base_price=6.34),
    Cushion(code='G', desc='Graphoil', base_price=6.34),
)


class Gasket(_PricedComponent):
    pass

gaskets = (
    Gasket(code='C', desc='C.A.F', base_price=7.34),
    Gasket(code='P', desc='P.T.F.E', base_price=7.34),
    Gasket(code='G', desc='Graphoil', base_price=7.34),
)


class IsolationValve(_PricedComponent):
    pass

isolation_valves = (
    IsolationValve(code='S', desc='Screwed Bonnet Offset Construction', base_price=15.69),
    IsolationValve(code='B', desc='Bolted Bonnet Offset Construction', base_price=15.69),
    IsolationValve(code='N', desc='Without Isolation Valve', base_price=0.00),
)


class Vent(_PricedComponent):
    pass

vents = (
    Vent(code='P', desc='0.5 in Plugged', base_price=3.90),
    Vent(code='N', desc='0.5 in Needle Valve', base_price=3.90),
    Vent(code='B', desc='0.5 in Ball Valve', base_price=3.90),
)


class Drain(_PricedComponent):
    pass

drains = (
    Drain(code='P', desc='0.5 in Plugged', base_price=4.90),
    Drain(code='N', desc='0.5 in Needle Valve', base_price=4.90),
    Drain(code='B', desc='0.5 in Ball Valve', base_price=4.90),
)


class CalibratedScale(_PricedComponent):
    pass

calibrated_scales = (
    CalibratedScale(code='AL', desc='Aluminium', base_price=25.00),
    CalibratedScale(code='SS', desc='SS', base_price=55.00),
)


class SpecialFeature(_PricedComponent):
    pass

special_features = (
    SpecialFeature(code='NF', desc='Non-Frost Extension', base_price=10.49),
    SpecialFeature(code='HJ', desc='Heating Jacket', base_price=10.49),
    SpecialFeature(code='MS', desc='Mica Shield(For TLG Only)', base_price=10.49),
    SpecialFeature(code='IW5', desc='Illuminator-Weatherproof IP-65', base_price=10.49),
    SpecialFeature(code='IW7', desc='Illuminator-Weatherproof IP-67', base_price=10.49),
    SpecialFeature(code='IFA', desc='Illuminator-Flameproof Gr. IIA/IIB', base_price=10.49),
    SpecialFeature(code='IFC', desc='Illuminator-Flameproof Gr. IIC', base_price=10.49),
    SpecialFeature(code='IC', desc='IBR Certification', base_price=25.49),
    SpecialFeature(code='NA', desc='Not Applicable', base_price=0.00),
)


class End(_Component):
    super().__init__(code='Z', desc='End')


class Separator(_Component):
    def __init__(self):
        super().__init__(code='-', desc='Product Code Separator')


component_metas = (
    ComponentMeta('Process Connection', None),
    ComponentMeta('Center To Center Distance', None),
    ComponentMeta('Flanged Connection', (('code1', 'Code'), ('size1', 'Size'), ('code2', 'Code'), ('code3', 'Code'), ('size2', 'Size'))),
    ComponentMeta('Screwed Connection', (('code1', 'Code'), ('size', 'Size'), ('code2', 'Code'), ('threading_type', 'Threading Type'))),
    ComponentMeta('MOC Of Liquid Chamber', None),
    ComponentMeta('MOC Of Cover Plate', None),
    ComponentMeta('Fastener', None),
    ComponentMeta('Toughened Borosilicate Glass', None),
    ComponentMeta('Cushion', None),
    ComponentMeta('Gasket', None),
    ComponentMeta('Isolation Valve', None),
    ComponentMeta('Vent', None),
    ComponentMeta('Drain', None),
    ComponentMeta('Calibrated Scale', None),
    ComponentMeta('Special Features', None),
    ComponentMeta('Separator', None),
    ComponentMeta('End', None)
)


class Product(object):
    def __init__(self, **kwargs):
        self.category = kwargs.get('category', kwargs['desc'])
        self.code = kwargs.get('code', None)
        self.desc = kwargs.get('desc', None)
        self._components = []

    def add_component(self, component):
        self._components.append(component)
        if isinstance(component, End):
            self._components = tuple(self._components)

    def price(self):
        return sum(c.price for c in self._components)

            
rlg = Product(code='RLG', desc='Reflex Level Guage', category='Liquid Level Guage')
tlg = Product(code='TLG', desc='Transparent Level Guage', category='Liquid Level Guage')
tlg = Product(code='TLG', desc='Tubular Level Guage')
mlg = Product(code='MLG', desc='Magnetic Level Guage')
difo = Product(code='DIFO', desc='Direct Insert Float Operated', category='Level Switch')
ecc = Product(code='ECC', desc='External Cage Chamber', category='Level Switch')
tmfo = Product(code='TMFO', desc='Top Mounted Float Operated', category='Level Switch')
tmdo = Product(code='TMDO', desc='Top Mounted Displacer Operated', category='Level Switch')
fa = Product(code='FA', desc='Flame Arrestor')
bv = Product(code='BV', desc='Breather Valve')
sfi = Product(code='SFI', desc='Sight Flow Indicator')
fs = Product(code='FS', desc='Flow Switch')
vafi = Product(code='VAFI', desc='Variable Area Flow Indicator')



rlg.add_component(Separator)
rlg.add_component(ProcessConnectionOrientation)
rlg.add_component(Separator)
rlg.add_component(CenterToCenterDistance)
rlg.add_component(Separator)
rlg.add_component(_ProcessConnection)
rlg.add_component(Separator)
rlg.add_component(LiquidChamberMOC)
rlg.add_component(CoverPlateMOC)
rlg.add_component(Fastener)
rlg.add_component(Separator)
rlg.add_component(ToughenedBorosilicateGlass)
rlg.add_component(Cushion)
rlg.add_component(Gasket)
rlg.add_component(Separator)
rlg.add_component(IsolationValve)
rlg.add_component(Vent)
rlg.add_component(Drain)
rlg.add_component(Separator)
rlg.add_component(CalibratedScale)
rlg.add_component(Separator)
rlg.add_component(SpecialFeature)
rlg.add_component(Separator)
rlg.add_component(End)