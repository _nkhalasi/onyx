def any_component_price_depends_on_moc(cdefs):
    return any(map(lambda cdef: cdef.price_depends_on.lower() == 'moc' if cdef.price_depends_on else False, cdefs))