from itertools import groupby
import math
 
# Based on Password Strength Meter posted at Snipplr
# http://snipplr.com/view/8755/password-strength-meter/
# and the corresponding python version by Henry Longmore.
 
punctuation = list("!@#$%^&* ()_+-='\";:[{]}\|.>,</?`~")
similarity_map = {
    '3': 'e', 'x': 'k', '5': 's', '$': 's', '6': 'g', '7': 't',\
    '8': 'b', '|': 'l', '9': 'g', '+': 't', '@': 'a', '0': 'o',
    '1': 'l', '2': 'z', '!': 'i'}

charsets = {
    'capital': lambda c : c.isalpha() and c == c.upper(),
    'lower': lambda c : c.isalpha() and c == c.lower(),
    'digit': lambda c : c.isdigit(),
    'punctuation': lambda c : c in punctuation }

common_seqs = [
    'abcdefghijklmnopqrstuvwxyz',
    '01234567890',
    'qwertyuiopasdfghjklzxcvbnm']
reverse_seqs = [ val[::-1] for val in common_seqs]
common_sequences = common_seqs + reverse_seqs 

score_chart = {
   (-1000, 0): 'Too Weak',
   (1, 299): 'Weak',
   (300, 699): 'Strong',
   (700, 1000): 'Very Strong'}


class PasswordStrengthCheck:
    def __init__(self):
        with open("/etc/dictionaries-common/words", "r") as dictionary:
            all_words = [line.strip().lower() for line in dictionary]
            all_words.sort(key=len)
        self.word_list = dict([(k, list(g)) for k, g in groupby(all_words, len)])
 
    def word_length(self, word):
        l = len(word)
        if l < 8:
            return 0
        if 8 < l < 14:
            return 1
        if 14 < l:
            return 2
        return 1

    def is_charset_type(self, c_class, c):
        return charsets[c_class](c)
 
    def charset_type_check(self, c_class):
        def inner(c):
            return self.is_charset_type(c_class, c)
        return inner
 
    def charset_span(self, word):
        checks = {'capital': 0, 'lower': 0, 'digit': 0, 'punctuation': 0}
        checker = lambda word, key : 1 if len(list(filter(self.charset_type_check(key), list(word)))) else 0
        return sum([checker(word, key) for key in checks.keys()])   
        
    def is_word_in_dictionary(self, word):
        return str(word) in self.word_list[len(word)]
    
    def is_subword_in_list(self, word, word_list, threshold):
        word_length = len(word)
        minMeaningfulMatch = int(math.floor(threshold * word_length))
        for length in range(minMeaningfulMatch, word_length):
            for start in range(0, word_length - minMeaningfulMatch):
                subword = word[start:start + length]
                if str(subword) in word_list[len(subword)]:
                    return True
        
    def is_partial_word_in_dictionary(self, word):
        return self.is_subword_in_list(word.lower(), self.word_list, 0.6)
    
    def is_partial_word_in_sequences(self, word):
        word_length = len(word)
        minMeaningfulMatch = int(math.floor(0.5 * word_length))
        for length in range(minMeaningfulMatch, word_length):
            for start in range(0, word_length - minMeaningfulMatch):
                subword = word[start:start + length]
                for seq in common_sequences:
                    if seq.find(subword) >= 0:
                        return True
        return False
    
    def canonical_char(self,c, letters_only=False):
        if letters_only and not self.is_charset_type('lower', c):
            return c
        else:
            return similarity_map.get(c, c)
        
    def canonicalize_word(self, word):
        canonicalizedWord = [self.canonical_char(c) for c in list(word.lower())]
        return ''.join(canonicalizedWord)
 
    def is_canonicalized_word_in_dictionary(self, word):
        canonicalized = self.canonicalize_word(word)
        word_length = len(canonicalized)
        return str(canonicalized) in self.word_list[word_length]
        
    def is_partial_canonicalized_word_in_dictionary(self, word):
        return self.is_subword_in_list(self.canonicalize_word(word), self.word_list, 0.6)
 
    def strength_check(self, password):
        if not password: return None,0
        if self.word_length(password) == 0:
            return 'Too Weak',0

        score = 500 * self.word_length(password)
        score -= 300 / self.charset_span(password)
        if self.is_word_in_dictionary(password):
            score -= 300
        if self.is_partial_word_in_dictionary(password):
            score -= 200
        if self.is_partial_word_in_sequences(password) :
            score -= 200
        if score and self.is_canonicalized_word_in_dictionary(password):
            score -= 200
        if score and self.is_partial_canonicalized_word_in_dictionary(password):
            score -= 100
        
        for rng, key in score_chart.items():
            if rng[0] <= score <= rng[1]:
                return key, score
        else:
            return None, score
    
    def __call__(self, password):
        return self.strength_check(password)
    
check_password_strength = PasswordStrengthCheck()