from decimal import Decimal
import json
import uuid
from flask import render_template, request, abort, flash, Response
from flask.ext.login import login_required
from onyx import app
from onyx.services.products import ProductService
from onyx.services.uploads import UploadJobService
import os
from werkzeug import secure_filename

import logging
from onyx.utils.uploads import UPLOAD_TASKS, allowed_file

LOG = logging.getLogger(__name__)


ZERO = Decimal('0.0')


def _pagination(req):
    limit = int(req.args['sz'] if req.args.get('sz', None) else '10')
    page_nbr = int(req.args['pg']) if req.args.get('pg', None) else 0
    offset = limit * page_nbr
    return limit, page_nbr, offset


@app.route('/products', methods=('GET', 'POST', 'PUT'))
@login_required
def products():
    limit, page_nbr, offset = _pagination(request)
    search_criteria = {
        'code': request.args.get('pc', None),
        'desc': request.args.get('pd', None),
    }
    ps_ = ProductService()
    return render_template('product_search.html',
                           products=ps_.search_products(search_criteria, limit, offset),
                           page_nbr=page_nbr, limit=limit)


@app.route('/components', methods=('GET', 'POST', 'PUT'))
@login_required
def components():
    limit, page_nbr, offset = _pagination(request)
    search_criteria = {
        'type': request.args.get('ct', None),
        'code': request.args.get('cc', None),
        'desc': request.args.get('cd', None),
    }
    ps_ = ProductService()
    return render_template('component_search.html',
                           components=ps_.search_components(search_criteria, limit, offset),
                           page_nbr=page_nbr, limit=limit)


@app.route('/costing', methods=('GET',))
@login_required
def costing():
    return render_template('costing_step1.html')


@app.route('/product_components', methods=('POST',))
@login_required
def product_components():
    code = request.form.get('pc', None)
    ps_ = ProductService()
    product = ps_.search_product(code)
    if product:
        return render_template('costing_step2.html', product=ps_.search_product(code))
    flash('No product found for {}'.format(code))
    return render_template('costing_step1.html', product_code=code)


@app.route('/calculate_cost', methods=('POST',))
@login_required
def calculate_cost():
    LOG.debug('Request params - {}'.format(request.values))
    product_code = request.form.get('pcode', None)
    ps_ = ProductService()
    product = ps_.search_product(product_code)
    errors = []
    if product:
        c2cd_field = request.form.get('c2cd_field', None)
        c2cd = int(request.form[c2cd_field]) if c2cd_field in request.form else 0
        all_codes = [product_code]
        components = []
        total_price = ZERO

        for idx, cdefs in product.component_defs_by_index:
            if cdefs[0].name == 'End':
                all_codes.append('Z')
                continue
            elif cdefs[0].name == 'Separator':
                all_codes.append('-')
                continue

            c_field_name = 'code{0:03d}'.format(idx)
            code_value = 'c2cd' if c_field_name == c2cd_field else request.form.get(c_field_name, None)
            moc_field_name = 'moc{0:03d}'.format(idx)
            moc_field_value = request.form.get(moc_field_name, None)
            component = ps_.search_component(code_value, moc_field_value, tuple(cdef.name for cdef in cdefs))
            if component:
                components.append(component)
                all_codes.append(str(c2cd) if component.is_center_to_center_distance else component.code)
                if c_field_name == c2cd_field:
                    total_price += (component.base_price * c2cd) / 1000
                elif component.fixed_priced:
                    total_price += component.base_price
                elif component.price_depends_on == 'c2cd':
                    total_price += (component.base_price * c2cd) / 1000
                elif component.price_depends_on == 'moc':
                    #TODO: TBD
                    total_price += component.base_price
                else:
                    abort(400)
            else:
                errors.append('Invalid code "{}" for "{}".'.format(code_value, '/'.join(cdef.name for cdef in cdefs)))
        if errors:
            for error in errors:
                flash(error)
            return render_template('costing_step2.html', product=product)
        else:
            return render_template('costing_step3.html', total_price=total_price,
                                   product=product, components=components,
                                   c2cd=c2cd, new_product_code=''.join(all_codes))
    else:
        flash('No product found for {}'.format(product_code))
        return render_template('costing_step1.html', product_code=product_code)


@app.route('/uploads', methods=('POST',))
@login_required
def upload_master_data():
    upload_type = request.form['upload_type']
    LOG.debug('Uploading {} files'.format(upload_type))
    file_processing_status = []
    ujs_ = UploadJobService()
    for _file in request.files.getlist('files'):
        try:
            LOG.debug(_file)
            if _file and allowed_file(_file.filename):
                _actual_filename = secure_filename(_file.filename)
                _storage_filename = os.path.join(app.config['UPLOAD_FOLDER'],
                                                 '{}.{}'.format(ujs_.context._auth_user.handle,
                                                                str(uuid.uuid4())))
                _file.save(_storage_filename)
                upload_job = ujs_.create_upload_job(upload_type, _actual_filename,
                                                    _storage_filename, upload_status='processing')
                try:
                    errors = UPLOAD_TASKS[upload_type](_storage_filename).process()
                    #force a database sync to detect database level constraint errors
                    # ujs_.context.db_session.flush()
                    all_errors = '\n'.join(errors) if errors else ''
                    LOG.debug('{} processing errors: {}'.format(_file.filename, all_errors))
                    if errors:
                        ujs_.context.db_session.rollback()
                        file_processing_status.append({'name': _file.filename, 'status': all_errors})
                        ujs_.update_job_status(upload_job, 'processing-failed', commit=True)
                    else:
                        file_processing_status.append({'name': _file.filename, 'status': 'done'})
                        upload_job = ujs_.update_job_status(upload_job, 'processed', commit=True)
                except Exception as e:
                    LOG.exception(e)
                    ujs_.context.db_session.rollback()
                    file_processing_status.append({'name': _file.filename, 'status': str(e)})
                    ujs_.update_job_status(upload_job, 'processing-failed', commit=True)
            else:
                LOG.debug('Invalid filename - {}'.format(_file.filename))
                file_processing_status.append({'name': _file.filename, 'status': 'Invalid filename'})
        except Exception as e:
            LOG.exception(e)
            ujs_.context.db_session.rollback()
            file_processing_status.append({'name': _file.filename, 'status': str(e)})

    ret = {"status": "success", "message": "Files have been processed", "files": file_processing_status}

    return Response(response=json.dumps(ret), status=200, mimetype="application/json")