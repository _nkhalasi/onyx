from flask import render_template, g
from flask.ext.login import login_required
from onyx import app


@app.route('/index')
@login_required
def index():
    return render_template('homepage.html', user=g.user)