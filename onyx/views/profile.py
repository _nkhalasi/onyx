from flask import render_template, g, request, Response, session, flash, redirect, url_for, abort
from flask.ext.login import login_required
from flask.views import MethodView
from onyx import app
from onyx.forms.profile import ProfileForm
from onyx.lib.password_strength_check import check_password_strength
import json
from onyx.services.authentication import AuthenticationService, salted_password_hash
from onyx.services.principals import UserService
import logging

LOG = logging.getLogger(__name__)


@app.route('/password_test', methods=('POST',))
def test_password():
    pwd = request.form.get('pwd', None)
    strength = 'Empty' if not pwd else check_password_strength(pwd)[0]
    ret = json.dumps({'strength': strength})
    return Response(response=ret, status=200, mimetype='application/json')


class ProfileView(MethodView):
    @login_required
    def get(self, id_):
        if not id_:
            abort(400)
        us_ = UserService()
        return render_template('profile.html', user=us_.find_user_by_id(id_), form=ProfileForm(csrf_context=session))

    @login_required
    def post(self):
        abort(405)

    @login_required
    def delete(self, id_):
        abort(405)

    @login_required
    def put(self, id_):
        form = ProfileForm(request.form, csrf_context=session)
        as_ = AuthenticationService()
        us_ = UserService()
        user = us_.find_user_by_id(id_)
        if form.validate():
            current_password = form.current_password.data
            if current_password and as_.is_authorized(user, current_password):
                user.email = form.email.data
                if form.new_password.data:
                    user.credentials = salted_password_hash(form.new_password.data, user.handle)
                us_.context.db_session.commit()
            else:
                flash('Unauthorized')
        else:
            LOG.debug('Profile form errors - {}'.format(form.errors))
            for f, e in form.errors.items():
                flash('{} - {}'.format(f, ' '.join(e)))
        us_.context.db_session.rollback()
        return render_template('profile.html', user=user, form=ProfileForm(csrf_context=session))

profile_view = ProfileView.as_view('user_profile')
app.add_url_rule('/profile/', defaults={'id_': None}, view_func=profile_view, methods=('GET',))
app.add_url_rule('/profile/', view_func=profile_view, methods=('POST',))
app.add_url_rule('/profile/<int:id_>', view_func=profile_view, methods=('GET', 'PUT', 'DELETE'))