from flask import render_template, g, redirect, url_for, request, session, flash
from flask.ext.login import LoginManager, login_user, current_user, logout_user
from onyx import app
from onyx.forms.login import LoginForm
from onyx.models import get_db_session
from onyx.services.authentication import AuthenticationService
import logging
from onyx.services.principals import UserService

LOG = logging.getLogger(__name__)


lm = LoginManager()
lm.init_app(app)


@lm.user_loader
def load_user(id):
    return UserService().find_user_by_id(int(id))


@app.route('/')
def login_page(form=None):
    if hasattr(g, 'user') and g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    return render_template('login.html', form=form or LoginForm(csrf_context=session))


@app.route('/authenticate', methods=('POST',))
def authenticate_user():
    if hasattr(g, 'user') and g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    form = LoginForm(request.form, csrf_context=session)
    if form.validate():
        as_ = AuthenticationService()
        verify_status, g.user = as_.verify_user_credentials(form.username.data, form.password.data)
        if verify_status:
            login_user(g.user, remember=form.remember_me.data)
            # flash('Welcome {}'.format(g.user.name))
            return redirect(request.args.get('next') or url_for('index'))
        else:
            flash('Invalid username or password')
    else:
        LOG.debug('Login form errors - {}'.format(form.errors))
        for f, e in form.errors.items():
            flash('{} - {}'.format(f, ' '.join(e)))
    # return render_template('login.html', form=form)
    return redirect(url_for('login_page'))


@app.before_request
def before_request():
    g.user = current_user


@app.teardown_appcontext
def shutdown_session(exception=None):
    get_db_session().remove()


@lm.unauthorized_handler
def unauthorized():
    flash('Login required. Please sign in.')
    return redirect(url_for('login_page'))


@app.route('/logout')
def logout():
    logout_user()
    g.user = None
    flash('You were logged out')
    return redirect(url_for('login_page'))