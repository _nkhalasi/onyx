from onyx.models import UploadJob
from onyx.services import _BaseService


class UploadJobService(_BaseService):
    def create_upload_job(self, upload_type, actual_filename, storage_filename,
                          uploaded_by=None, upload_status='received'):
        upload_job = UploadJob(upload_type=upload_type, upload_filename=actual_filename,
                               storage_filename=storage_filename,
                               upload_user=uploaded_by if uploaded_by else self.context._auth_user,
                               upload_status=upload_status)
        self.context.db_session.add(upload_job)
        self.context.db_session.commit()
        return upload_job

    def update_job_status(self, upload_job, new_status, commit=False):
        upload_job.upload_status = new_status
        if commit:
            self.context.db_session.commit()
        return upload_job