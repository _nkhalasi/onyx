from onyx.models import User
from onyx.services import _BaseService
import random
import string

PASSWORD_SALT_LENGTH = 6


def compute_hash(password, user_handle):
        from hashlib import sha512
        m = sha512()
        m.update(password.encode("utf-8"))
        m.update(user_handle.encode("utf-8"))
        return m.hexdigest()


def salted_password_hash(password, user_handle):
    salt = ''.join(random.choice(string.ascii_letters + string.digits + string.punctuation)
           for _ in range(PASSWORD_SALT_LENGTH))
    return salt + compute_hash(salt + password, user_handle)


class AuthenticationService(_BaseService):
    def verify_user_credentials(self, user, password):
        if isinstance(user, (str, unicode)):
            user = self.context.db_session.query(User).filter(User.handle == user).first()
        if user:
            salt = user.credentials[:PASSWORD_SALT_LENGTH]
            return user.credentials[PASSWORD_SALT_LENGTH:] == compute_hash(salt + password, user.handle), user
        return False, None

    def is_authorized(self, user, password):
        auth_status, _ = self.verify_user_credentials(user, password)
        return auth_status
