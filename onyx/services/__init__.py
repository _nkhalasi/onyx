from onyx.context import new_context


class _BaseService(object):
    def __init__(self, context=None, **kwargs):
        self.context = context or new_context()
        self.__dict__.update(kwargs)