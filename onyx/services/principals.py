from onyx.models import User
from onyx.services import _BaseService


class UserService(_BaseService):
    def find_user_by_id(self, id):
        return User.find_by_id(self.context, id)