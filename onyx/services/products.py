from onyx.models import Product, Component, ComponentDef, ProductComponent
from onyx.services import _BaseService
import logging

LOG = logging.getLogger(__name__)


class ProductService(_BaseService):
    def create_product(self, product):
        self.context.db_session.add(product)

    def find_by_code(self, product_code):
        return self.context.db_session.query(Product).filter(Product.code == product_code).first()

    def search_products(self, criteria, limit, offset):
        query = self.context.db_session.query(Product)
        if criteria['code']:
            query = query.filter(Product.code == criteria['code'])
        if criteria['desc']:
            query = query.filter(Product.desc.like('%{}%'.format(criteria['desc'])))
        if offset:
            query = query.offset(offset)
        if limit:
            query = query.limit(limit)
        return query.all()

    def search_components(self, criteria, limit, offset):
        query = self.context.db_session.query(Component)
        if criteria['type']:
            query = query.join(ComponentDef).filter(ComponentDef.name == criteria['type'])
        if criteria['code']:
            query = query.filter(Component.code == criteria['code'])
        if criteria['desc']:
            query = query.filter(Component.desc.like('%{}%'.format(criteria['desc'])))
        if offset:
            query = query.offset(offset)
        if limit:
            query = query.limit(limit)
        return query.all()

    def search_product(self, code):
        return self.context.db_session.query(Product).filter(Product.code == code).first()

    def search_component(self, code, moc, component_def_names):
        if isinstance(component_def_names, str):
            component_def_names = (component_def_names,)
        if moc is not None and moc.strip() == '':
            moc = None
        return self.context.db_session.query(Component).join(ComponentDef).\
            filter(Component.code == code).\
            filter(Component.moc == moc).\
            filter(ComponentDef.name.in_(component_def_names)).\
            first()

    def search_component_def(self, name):
        return self.context.db_session.query(ComponentDef).filter(ComponentDef.name == name).first()

    def create_component_def(self, name, price_depends_on=None):
        cdef = ComponentDef(name=name, price_depends_on=price_depends_on if price_depends_on else None)
        self.context.db_session.add(cdef)
        return cdef

    def create_component(self, code, desc, moc, base_price, cdef):
        comp = Component(code=code, desc=desc, moc=moc if moc else None, base_price=base_price, component_def=cdef)
        self.context.db_session.add(comp)
        return comp

    def search_product_component_def_mapping(self, pcode, cname, idx):
        query = self.context.db_session.query(ProductComponent).filter(ProductComponent.component_index == idx)
        query = query.join(Product).filter(Product.code == pcode)
        query = query.join(ComponentDef).filter(ComponentDef.name == cname)
        return query.first()