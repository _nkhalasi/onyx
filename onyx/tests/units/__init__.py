import unittest
import onyx
from onyx.context import new_context
from onyx.models import Base, get_db_session
from onyx.utils import config
from sqlalchemy import create_engine


def clear_tables(db_session, condition=lambda t: True):
    for t in Base.metadata.sorted_tables:
        if condition(t):
            db_session.execute(t.delete())
    db_session.commit()


TXN_TABLES = ('products',)


def is_txn_table(table):
    return table.name in TXN_TABLES


class _BaseOnyxTestCase(unittest.TestCase):
    def setUp(self):
        super().setUp()
        onyx.initialize_flask_app('development.ini')
        onyx.app.config['TESTING'] = True
        onyx.app.config['CSRF_ENABLED'] = False
        self.app = onyx.app.test_client()

        self.context = new_context()
        clear_tables(self.context.db_session, is_txn_table)

    def tearDown(self):
        if self.context and self.context.db_session:
            self.context.db_session.close()
        self.context = None