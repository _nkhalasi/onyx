from onyx.tests.units import _BaseOnyxTestCase


class LoginViewTestCase(_BaseOnyxTestCase):
    def test_login_page(self):
        rv = self.app.get('/')
        rv_data = rv.data.decode('utf-8')
        self.assertNotEqual(rv_data.find('<input id="csrf_token" name="csrf_token" type="hidden" '), -1)
        self.assertNotEqual(rv_data.find('<legend class="text-center">Sign In</legend>'), -1)
        self.assertNotEqual(rv_data.find('<input class="span10" type="text" name="username" id="username" autocomplete="off" placeholder="Username">'), -1)
        self.assertNotEqual(rv_data.find('<input class="span10" name="password" id="password"  type="password" autocomplete="off" placeholder="Password">'), -1)
        self.assertNotEqual(rv_data.find('<button class="btn btn-primary" id="submitbtn" type="submit" name="Submit"><i class="icon-signin"></i>&nbsp;Sign In</button>'), -1)

    def login(self, username, password):
        return self.app.post('/authenticate', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def test_login_logout(self):
        rv = self.login('onyxadmin', 'admin123')
        assert '<i class="icon-user"></i>&nbsp;Onyx Administrator <span class="caret"></span>' in rv.data.decode('utf-8')
        rv = self.logout()
        assert 'You were logged out' in rv.data.decode('utf-8')
        rv = self.login('admin', 'admin')
        assert 'Invalid username or password' in rv.data.decode('utf-8')
        rv = self.login('oynxadmin', 'admin')
        assert 'Invalid username or password' in rv.data.decode('utf-8')