from onyx.models import Product
from onyx.services.products import ProductService
from onyx.tests.units import _BaseOnyxTestCase


class ProductServiceTestCase(_BaseOnyxTestCase):
    def test_should_create_product_in_database(self):
        p = Product(code='p1', desc='Product One')
        ps = ProductService(self.context)
        ps.create_product(p)
        self.context.db_session.commit()
        p = ps.find_by_code('p1')
        self.assertEqual(p.code, 'p1')
        self.assertEqual(p.desc, 'Product One')

    def test_should_create_multiple_products_in_database(self):
        ps = ProductService(self.context)
        ps.create_product(Product(code='p1', desc='Product One'))
        ps.create_product(Product(code='p2', desc='Product Two'))
        self.context.db_session.commit()
        p = ps.find_by_code('p2')
        self.assertEqual(p.code, 'p2')
        self.assertEqual(p.desc, 'Product Two')
        p = ps.find_by_code('p1')
        self.assertEqual(p.code, 'p1')
        self.assertEqual(p.desc, 'Product One')