import argparse
import logging
import json
from onyx.models import Principal, Product, ComponentDef, Component, ProductComponent
from onyx.services.authentication import salted_password_hash

LOG = logging.getLogger('onyx.tests.scripts.database_setup')


def recreate_schema(engine):
    from onyx import models

    metadata = models.Base.metadata
    metadata.drop_all(engine)
    metadata.create_all(engine)


def create_admin_user(db_session):
    admin_user = Principal(handle='onyxadmin', name='Onyx Administrator', email='naresh.khalasi@gmail.com',
                           role='admin', credentials=salted_password_hash('admin123', 'onyxadmin'))
    db_session.add(admin_user)


def create_default_components(db_session):
    db_session.add_all(
        [
            ComponentDef(name='Separator', specs=None),
            ComponentDef(name='End', specs=None)
        ]
    )
    db_session.add_all(
        [
            Component(code='Z', desc='End',
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'End').first()),
            Component(code='-', desc='Separator',
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Separator').first()),
        ]
    )


def create_masters(db_session):
    db_session.add_all(
        [
            Product(code='RLG', desc='Reflex Level Guage', category='Liquid Level Guage'),
            Product(code='TLG', desc='Transparent Level Guage', category='Liquid Level Guage'),
            Product(code='TULG', desc='Tubular Level Guage'),
            Product(code='MLG', desc='Magnetic Level Guage'),
            Product(code='DIFO', desc='Direct Insert Float Operated', category='Level Switch'),
            Product(code='ECC', desc='External Cage Chamber', category='Level Switch'),
            Product(code='TMFO', desc='Top Mounted Float Operated', category='Level Switch'),
            Product(code='TMDO', desc='Top Mounted Displacer Operated', category='Level Switch'),
            Product(code='FA', desc='Flame Arrestor'),
            Product(code='BV', desc='Breather Valve'),
            Product(code='SFI', desc='Sight Flow Indicator'),
            Product(code='FS', desc='Flow Switch'),
            Product(code='VAFI', desc='Variable Area Flow Indicator'),
        ]
    )
    db_session.add_all(
        [
            ComponentDef(name='Orientation Of Process Connection', specs=None),
            ComponentDef(name='Center To Center Distance', specs=None),
            ComponentDef(name='Flanged Connection', price_depends_on='moc',
                         specs=json.dumps((('code1', 'Code'), ('size1', 'Size'), ('code2', 'Code'), ('code3', 'Code'),
                                           ('size2', 'Size')))),
            ComponentDef(name='Screwed Connection',
                         specs=json.dumps((('code1', 'Code'), ('size', 'Size'), ('code2', 'Code'),
                                           ('threading_type', 'Threading Type')))),
            ComponentDef(name='MOC Of Liquid Chamber', price_depends_on='c2cd', specs=None),
            ComponentDef(name='MOC Of Cover Plate', price_depends_on='c2cd', specs=None),
            ComponentDef(name='Fastener', price_depends_on='c2cd', specs=None),
            ComponentDef(name='Toughened Borosilicate Glass', price_depends_on='c2cd', specs=None),
            ComponentDef(name='Cushion', price_depends_on='c2cd', specs=None),
            ComponentDef(name='Gasket', price_depends_on='c2cd', specs=None),
            ComponentDef(name='Isolation Valve', price_depends_on='moc', specs=None),
            ComponentDef(name='Vent', price_depends_on='moc', specs=None),
            ComponentDef(name='Drain', price_depends_on='moc', specs=None),
            ComponentDef(name='Calibrated Scale', price_depends_on='c2cd', specs=None),
            ComponentDef(name='Special Features', specs=None),
        ]
    )
    db_session.add_all(
        [
            Component(code='NF', desc='Non-Frost Extension', base_price=10.49,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Special Features').first()),
            Component(code='HJ', desc='Heating Jacket', base_price=10.49,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Special Features').first()),
            Component(code='MS', desc='Mica Shield(For TLG Only)', base_price=10.49,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Special Features').first()),
            Component(code='IW5', desc='Illuminator-Weatherproof IP-65', base_price=10.49,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Special Features').first()),
            Component(code='IW7', desc='Illuminator-Weatherproof IP-67', base_price=10.49,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Special Features').first()),
            Component(code='IFA', desc='Illuminator-Flameproof Gr. IIA/IIB', base_price=10.49,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Special Features').first()),
            Component(code='IFC', desc='Illuminator-Flameproof Gr. IIC', base_price=10.49,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Special Features').first()),
            Component(code='IC', desc='IBR Certification', base_price=25.49,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Special Features').first()),
            Component(code='NA', desc='Not Applicable', base_price=0.00,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Special Features').first()),
            Component(code='AL', desc='Aluminium', base_price=25.00,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Calibrated Scale').first()),
            Component(code='SS', desc='SS', base_price=55.00,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Calibrated Scale').first()),
            Component(code='P', desc='0.5 in Plugged', base_price=4.90, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Drain').first()),
            Component(code='N', desc='0.5 in Needle Valve', base_price=4.90, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Drain').first()),
            Component(code='B', desc='0.5 in Ball Valve', base_price=4.90, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Drain').first()),
            Component(code='P', desc='0.5 in Plugged', base_price=3.90, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Vent').first()),
            Component(code='N', desc='0.5 in Needle Valve', base_price=3.90, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Vent').first()),
            Component(code='B', desc='0.5 in Ball Valve', base_price=3.90, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Vent').first()),
            Component(code='S', desc='Screwed Bonnet Offset Construction', base_price=15.69, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Isolation Valve').first()),
            Component(code='B', desc='Bolted Bonnet Offset Construction', base_price=15.69, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Isolation Valve').first()),
            Component(code='N', desc='Without Isolation Valve', base_price=0.00,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Isolation Valve').first()),
            Component(code='C', desc='C.A.F', base_price=7.34,
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Gasket').first()),
            Component(code='P', desc='P.T.F.E', base_price=7.34,
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Gasket').first()),
            Component(code='G', desc='Graphoil', base_price=7.34,
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Gasket').first()),
            Component(code='C', desc='C.A.F', base_price=6.34,
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Cushion').first()),
            Component(code='P', desc='P.T.F.E', base_price=6.34,
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Cushion').first()),
            Component(code='G', desc='Graphoil', base_price=6.34,
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Cushion').first()),
            Component(code='L', desc='Indigenous', base_price=5.50,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Toughened Borosilicate Glass').first()),
            Component(code='F', desc='Klinger/Illmadur make or equivalent', base_price=5.50,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Toughened Borosilicate Glass').first()),
            Component(code='C', desc='ASTM A 193 Gr. B7 / ASTM A 194 Gr. 2H', base_price=4.17,
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Fastener').first()),
            Component(code='SS', desc='SS', base_price=4.17,
                      component_def=db_session.query(ComponentDef).filter(ComponentDef.name == 'Fastener').first()),
            Component(code='C', desc='CS', base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='S4', desc='SS 304', base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='S4L', desc='SS 304L', base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='S6', desc='SS 316', base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='S6L', desc='SS 316L', base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='PP', desc='Polypropylene', base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='M', desc='Monel', base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='T', desc='Titanium', base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='I', desc='Inconel 600', base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='H', desc="Hastolley 'C'", base_price=4.23,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Cover Plate').first()),
            Component(code='C', desc='CS', base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='S4', desc='SS 304', base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='S4L', desc='SS 304L', base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='S6', desc='SS 316', base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='S6L', desc='SS 316L', base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='PP', desc='Polypropylene', base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='M', desc='Monel', base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='T', desc='Titanium', base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='I', desc='Inconel 600', base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='H', desc="Hastolley 'C'", base_price=6.10,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'MOC Of Liquid Chamber').first()),
            Component(code='15F150', desc='Flanged Connection 15F150', base_price=5.75, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Flanged Connection').first()),
            Component(code='20F300', desc='Flanged Connection 20F300', base_price=5.75, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Flanged Connection').first()),
            Component(code='25F600', desc='Flanged Connection 25F600', base_price=5.75, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Flanged Connection').first()),
            Component(code='40F900', desc='Flanged Connection 40F900', base_price=5.75, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Flanged Connection').first()),
            Component(code='50F1500', desc='Flanged Connection 50F1500', base_price=5.75, moc='SS',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Flanged Connection').first()),
            Component(code='SNPTM', desc='Screwed Connection SNPTM', base_price=5.75,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Screwed Connection').first()),
            Component(code='SBSPM', desc='Screwed Connection SBSPM', base_price=5.75,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Screwed Connection').first()),
            Component(code='SNPTF', desc='Screwed Connection SNPTF', base_price=5.75,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Screwed Connection').first()),
            Component(code='SBSPF', desc='Screwed Connection SBSPF', base_price=5.75,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Screwed Connection').first()),
            Component(code='c2cd', desc='Center to center distance in mm', base_price=10.35,
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Center To Center Distance').first()),
            Component(code='TBV', desc='Top-Bottom Vertical(Partial Visibility)',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Orientation Of Process Connection').first()),
            Component(code='SSR', desc='Side-Side Right(Full Visibility)',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Orientation Of Process Connection').first()),
            Component(code='SSL', desc='Side-Side Left(Full Visibility)',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Orientation Of Process Connection').first()),
            Component(code='SSB', desc='Side-Side Back(Full Visibility)',
                      component_def=db_session.query(ComponentDef).filter(
                          ComponentDef.name == 'Orientation Of Process Connection').first()),
        ]
    )
    db_session.commit()


def product_compositions(db_session):
    pc = {
        'TLG': (('Separator', 1), ('Orientation Of Process Connection', 2), ('Separator', 3),
                ('Center To Center Distance', 4), ('Separator', 5), ('Flanged Connection|Screwed Connection', 6),
                ('Separator', 7), ('MOC Of Liquid Chamber', 8), ('MOC Of Cover Plate', 9), ('Fastener', 10),
                ('Separator', 11), ('Toughened Borosilicate Glass', 12), ('Cushion', 13), ('Gasket', 14),
                ('Separator', 15), ('Isolation Valve', 16), ('Vent', 17), ('Drain', 18), ('Separator', 19),
                ('Calibrated Scale', 20), ('Separator', 21), ('Special Features', 22), ('Separator', 23), ('End', 24))
    }

    for pcode, comp_list in pc.items():
        product = db_session.query(Product).filter(Product.code == pcode).first()
        for cnames, idx in comp_list:
            cnames = cnames.split('|')
            for cname in cnames:
                component_def = db_session.query(ComponentDef).filter(ComponentDef.name == cname).first()
                pcomponent = ProductComponent(component_index=idx)
                pcomponent.component_def = component_def
                product.component_defs.append(pcomponent)

    db_session.commit()


def main():
    try:
        parser = argparse.ArgumentParser(description='Manage ONYX database')
        parser.add_argument('-s', '--reset-schema', help='recreate schema', action='store_true')
        parser.add_argument('config_file', help='Configuration file e.g. test.ini')
        args = parser.parse_args()

        from onyx.utils import load_config, enable_logging, database_config
        from onyx.models import get_db_session
        from sqlalchemy.engine import create_engine

        load_config(args.config_file)
        enable_logging(args.config_file)
        db_engine = create_engine(database_config()['url'])
        db_session = get_db_session(db_engine)

        if args.reset_schema:
            recreate_schema(db_engine)
            create_admin_user(db_session)
            create_default_components(db_session)
            # create_masters(db_session)
            # product_compositions(db_session)

        db_session.commit()
        LOG.debug('{_10stars} Reset DB is done {_10stars}'.format(_10stars='*' * 10))
    except Exception as e:
        LOG.exception(e)


if __name__ == '__main__':
    main()
