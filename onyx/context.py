class Context(object):
    def __init__(self, db_session, db_meta, auth_user, config):
        self.db_session = db_session
        self.db_meta = db_meta
        self._auth_user = auth_user
        self.config = config

    @property
    def user(self):
        return self._auth_user

    @user.setter
    def user(self, auth_user):
        self._auth_user = auth_user


def build_context(db_session, db_meta, auth_user, config):
    return Context(db_session, db_meta, auth_user, config)


def new_context():
    from onyx.models import get_db_session, Base
    from onyx.utils import config as app_config
    from flask import g
    return build_context(get_db_session(), Base.metadata, getattr(g, 'user', None), app_config)