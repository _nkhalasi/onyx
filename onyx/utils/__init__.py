config = None


def enable_logging(config_uri):
    import logging.config
    logging.config.fileConfig(config_uri)


def load_config(config_uri):
    import ConfigParser
    global config
    config = ConfigParser.ConfigParser()
    config.read(config_uri)
    return config


def _filter_config(config, section, prefix):
    #return {k[len(prefix):]: v for k, v in config[section].items() if k.startswith(prefix)}
    return {k[len(prefix):]: v for k, v in config.items(section) if k.startswith(prefix)}

# database_config is a method since config is lazily loaded when load_config() is invoked
database_config = lambda : _filter_config(config, 'app:main', 'sqlalchemy.')
uploads_config = lambda : _filter_config(config, 'app:main', 'uploads.')
