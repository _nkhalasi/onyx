import csv
import threading
from Queue import Queue
import logging
import itertools
from onyx.models import Product, ProductComponent
from onyx.services.products import ProductService

LOG = logging.getLogger(__name__)


UPLOAD_QUEUE = Queue()
ALLOWED_EXTENSIONS = ('csv',)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


class _UploadTask(object):
    def __init__(self, name):
        self.name = name


class _UploadFileTask(_UploadTask):
    def __init__(self, name, upload_file):
        super(_UploadFileTask, self).__init__(name)
        self.upload_file = upload_file


class ProductUploadTask(_UploadFileTask):
    ## File layout -
    ## Product Category, Product Code, Product Description
    def __init__(self, upload_file):
        super(ProductUploadTask, self).__init__('products_upload', upload_file)

    def process(self):
        errors = []
        ps_ = ProductService()
        with open(self.upload_file) as f:
            reader = csv.reader(f)
            for row in reader:
                prod = ps_.search_product(row[1])
                if not prod:
                    LOG.debug('Creating product - {}'.format(row))
                    ps_.create_product(Product(category=row[0], code=row[1], desc=row[2]))
        return errors


class ProductComponentsMapUploadTask(_UploadFileTask):
    def __init__(self, upload_file):
        super(ProductComponentsMapUploadTask, self).__init__('component_types_upload', upload_file)

    def process(self):
        errors = []
        ps_ = ProductService()
        with open(self.upload_file) as f:
            reader = csv.reader(f)
            pc_map_data = itertools.groupby(sorted(reader, key=lambda pcdata: pcdata[0]),
                                            key=lambda pcdata: pcdata[0] )

        if pc_map_data:
            for pcode, product_components in pc_map_data:
                LOG.debug(pcode)
                product = ps_.search_product(pcode)
                if not product:
                    errors.append('No product found for code={}'.format(pcode))
                    continue

                for idx, pc_row in itertools.groupby(sorted(product_components, key=lambda pcdata: int(pcdata[1])),
                                                     key=lambda pcdata: int(pcdata[1])):
                    for pc_data in pc_row:
                        component_def = ps_.search_component_def(pc_data[2])
                        if not component_def:
                            errors.append('No component found for product={} and component={}'.format(pcode, pc_data[2]))
                        else:
                            pc_map = ps_.search_product_component_def_mapping(pcode, pc_data[2], pc_data[1])
                            if pc_map:
                                LOG.debug('{} already exists'.format(pc_data))
                            else:
                                LOG.debug('Creating product component map for {}'.format(pc_data))
                                product.component_defs.append(ProductComponent(component_index=idx,
                                                                               component_def=component_def))

        return errors


class ComponentUploadTask(_UploadFileTask):
    ## File layout -
    ## Component Name, Price depends on('', c2cd, moc), Component Code, Component Description, MOC, Base Price
    def __init__(self, upload_file):
        super(ComponentUploadTask, self).__init__('components_upload', upload_file)

    def process(self):
        errors = []
        with open(self.upload_file) as f:
            reader = csv.reader(f)
            components_data = itertools.groupby(sorted(reader, key=lambda cdata: (cdata[0], cdata[1])),
                                                key=lambda cdata: (cdata[0], cdata[1]))

        ps_ = ProductService()
        if components_data:
            for comp_def, comp_data_row in components_data:
                comp_name, comp_price_depends_on = comp_def
                comp_name = comp_name.strip()
                comp_price_depends_on = comp_price_depends_on.strip().lower()

                cdef = ps_.search_component_def(comp_name)
                if not cdef:
                    if comp_price_depends_on in ('', None, 'moc', 'c2cd'):
                        LOG.debug('Creating new component definition - {}'.format(comp_def))
                        cdef = ps_.create_component_def(comp_name, comp_price_depends_on)
                    else:
                        errors.append('{}:: Price depends on field should be either empty, moc or c2cd'.format(comp_def))

                for comp_data in comp_data_row:
                    if comp_price_depends_on == 'moc' and not comp_data[4].strip():
                        errors.append('{}:: MOC is mandatory when component price depends on MOC'.format((comp_name,
                                                                                                          comp_data[2])))
                    else:
                        comp = ps_.search_component(comp_data[2].strip(), comp_data[4].strip(), (comp_name,))
                        if not comp:
                            LOG.debug('Creating new component - {}'.format(comp_data))
                            ps_.create_component(comp_data[2], comp_data[3], comp_data[4], comp_data[5], cdef)
        return errors

UPLOAD_TASKS = {
    'products': ProductUploadTask,
    'product_components': ProductComponentsMapUploadTask,
    'components': ComponentUploadTask
}


class _BaseUploadWorker(threading.Thread):
    def __init__(self):
        super(_BaseUploadWorker, self).__init__()
        self.STOP_SLUG = object()


class UploadFileWorker(_BaseUploadWorker):
    def __init__(self, db_engine=None):
        super(UploadFileWorker, self).__init__()
        self.db_engine = db_engine

    def run(self):
        from onyx.models import get_db_session
        db_session = get_db_session(self.db_engine)
        while True:
            upload_file_task = UPLOAD_QUEUE.get()
            if upload_file_task == self.STOP_SLUG:
                break

            try:
                upload_file_task.process()
            finally:
                db_session.remove()
