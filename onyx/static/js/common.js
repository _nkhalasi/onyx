
function redirectSelection(e){
	e.preventDefault();
	try {
		$(this).siblings('input[type="file"]').click();
	}catch(err){}
}

function fileListRow(filename, lbl){
	$('<tr><td><span class="pull-left">'+filename+'</span></td></tr>').appendTo(
			$('table.selected-files tbody'));
}

function filesSelected(){
	// When original file input changes, get its value, show it in
	// the fake input
	var pF = $(this).parents('.prettyFile')[0];
	var info = '1 file selected';
	$(pF).find('table.selected-files tbody').empty();
	$(pF).find('table.selected-files').hide();
	if (Modernizr.input['multiple']) {
		var files = this.files;
		if (files.length > 1) {
			info = files.length + ' files selected';
			for (var i=0; i<files.length; i++) {
				fileListRow(files.item(i).name);
			}
		} else {
			fileListRow($(this).val().split('/').pop().split('\\').pop());
		}
	} else {
		fileListRow($(this).val().split('/').pop().split('\\').pop());
	}
	$('table.selected-files').show();
	$(pF).find('.input-append label.file-label').text(info);
}

function resetFileUploads(elem) {
	var pF = $(elem);
	pF.find('table.selected-files').hide();
	pF.find('table.selected-files tbody').empty();
	if (Modernizr.input['multiple']) {
		pF.find('input[type="file"]').remove();
		$('<input id="files" type="file" name="files" class="file-input" multiple="multiple"/>').insertAfter('.input-append label.selects');
		pF.find('.input-append label.file-label').text("");
		pF.find('.input-append label.selects, .input-append label.file-label').unbind().click(redirectSelection);
		pF.find('input[type="file"]').unbind().change(filesSelected);
		pF.find('a.clears').unbind().click(function(){
			resetFileUploads($(this).parents('.prettyFile')[0]);
		});
	} else {
		pF.find('.input-append label.selects, .input-append label.file-label').hide();
		pF.find('input[type="file"]').removeClass("file-input");
		pF.find('input[type="file"]').val('');
	}
}

function setupFileUploads() {
	if ($('.prettyFile').length) {
		$('.prettyFile').each(function(){
			resetFileUploads(this);
		});
	}
}

function setupUploadMasterDialog() {
    $('#uploadModal').on('show',function(e){
   		$('span#uploadMessage').empty();
   		$(this).find('div.alert').removeClass('alert-error').removeClass('alert-success').addClass("hidden");
   		setupFileUploads();
   	});
}

function setupUploadMasterProcessingEvents() {
    $('.submit-uploads').click(function(e){
   		e.preventDefault();
   		$('form#uploads').ajaxSubmit({
   			type:'POST', dataType:'json',
   			beforeSubmit: function(){
   				$('.submit-uploads').button('loading');
   				$('form#uploads').find('div.alert').removeClass('alert-error').removeClass('alert-success').addClass("hidden");
   			},
   			success: function(response, statusText, xhr, frm){
//                for (var key in response) {
//                    alert('Key is ' + key);
//                }
   				if (response.status == 'error'){
   					$('span#uploadMessage').text(response.message);
   					frm.find('div.alert').removeClass('alert-success').addClass('alert-error').removeClass('hidden');
   				} else if(response.files !== 'undefined'){
   					for(var i=0; i<response.files.length; i++){
   						var f = response.files[i];
   						if(f.status=='done')
   							var lbl = '<span class="label label-success pull-right">Uploaded</span>';
                        else {
                            var fstatus = f.status.split('\n');
                            var st = '';
                            for (var i in fstatus) {
                                st += "<li>" + fstatus[i] + "</li>";
                            }
                            var lbl = '<span class="label label-important pull-right">'+st+'</span>';
                        }

   						if ($('.prettyFile table.selected-files').is(':visible')){
   							frm.find('table.selected-files td:contains("'+f.name+'")').append(lbl);
   						} else {
   							fileListRow(f.name);
   							frm.find('a.clears').hide();
   							frm.find('table.selected-files td:contains("'+f.name+'")').append(lbl).append('<span class="clearfix"></span>');
   							$('.prettyFile table.selected-files').show();
   						}
   					}
   				}
   				$('.submit-uploads').button('reset');
   			},
   			error:function(xhr, status, error){
                alert('error');
   				var response = '';
   				try{
   					var res = $.parseJSON(xhr.responseText);
   					response = res.message;
   				}catch(e){}
   				if (response !== ''){
   					$('span#uploadMessage').text(response);
   				} else {
   					$('span#uploadMessage').text("Oops! Something went wrong, please try again later.");
   				}
   				$('form#uploads').find('div.alert').removeClass('alert-success').addClass('alert-error').removeClass('hidden');
   				$('.submit-uploads').button('reset');
   				$("div.loading_modal").hide();
   			}
   		});
   	});
}

function productListingSetup(data){
	$.placeholder();
	$(".chzn-select-deselect").chosen({allow_single_deselect:true});
	$('.footable').footable();
	$("#prodSearchBtn").click(function(e){
		e.preventDefault();
		$(this).button('loading');
		loadProducts();
	});
	$("a.pgr-link").click(function(e){
		e.preventDefault();
		loadProducts($(this).attr("page_target"));
	});

    setupUploadMasterDialog();
    setupUploadMasterProcessingEvents();
}

// Product Listing Loading
function loadProducts(pg){
	var data = {}
	if (undefined === pg) pg = 0;
	if ($('#pSearch')) {
		data = $('#pSearch').serialize();
    }
	$.ajax({
		url:'/products?pg='+pg,
		type:"GET",
		cache:false,
		data:data
	}).done(function(html){
		$("#content").hide('slide', function() {
			slideIn(this, html,
					function(){
						productListingSetup(data);
					});
		});
	});
}

function loadComponents(pg){
	var data = {}
	if (undefined === pg) pg = 0;
	if ($('#cSearch')) {
		data = $('#cSearch').serialize();
    }
	$.ajax({
		url:'/components?pg='+pg,
		type:"GET",
		cache:false,
		data:data
	}).done(function(html){
		$("#content").hide('slide', function() {
			slideIn(this, html,
					function(){
						componentSetup(data);
					});
		});
	});
}

function componentSetup() {
	$('.footable').footable();
    $("#compSearchBtn").click(function(e){
   		e.preventDefault();
   		$(this).button('loading');
        loadComponents();
   	});
   	$("a.pgr-link").click(function(e){
   		e.preventDefault();
        loadComponents($(this).attr("page_target"));
   	});

    setupUploadMasterDialog();
    setupUploadMasterProcessingEvents();
}

function loadCostingStep1Screen(){
	var data = {}
	$.ajax({
		url:'/costing',
		type:"GET",
		cache:false,
		data:data
	}).done(function(html){
		$("#content").hide('slide', function() {
			slideIn(this, html);
		});
	});
}

function setupCostingStep1Screen() {
    $("#psearchBtn").click(function(e){
   		e.preventDefault();
   		$(this).button('loading');
        loadCostingStep2Screen();
        $(this).button('reset');
   	});

    setupUploadMasterDialog();
    setupUploadMasterProcessingEvents();
}

function loadCostingStep2Screen() {
    var data = {}
    if ($('#psearch')) {
        data = $('#psearch').serialize();
    }
    $.ajax({
        url: '/product_components',
        type: 'POST',
        cache: false,
        data: data
    }).done(function(html) {
        $("#content").hide('slide', function() {
            slideIn(this, html);
        });
    });
}

function setupCostingStep2CalculateButton() {
    $("#costingBtn").click(function(e){
   		e.preventDefault();
   		$(this).button('loading');
        loadCostingResultScreen();
        $(this).button('reset');
   	});
}

function loadCostingResultScreen() {
    var data = {}
    if ($('#costing-form')) {
        data = $('#costing-form').serialize();
    }
    $.ajax({
        url: '/calculate_cost',
        type: 'POST',
        cache: false,
        data: data
    }).done(function(html) {
        $('#content').hide('slide', function() {
            slideIn(this, html);
        });
    });
}

function setupPasswordTest() {
	$('#new-password').change(function(){
		var clss = {'Empty':'', 'Too Weak':'label-important', 'Weak':'label-warning', 'Strong':'label-info', 'Very Strong':'label-success'};
		$('.password-strength').removeClass(function (index, css) {
		    return (css.match (/\blabel-\S+/g) || []).join(' ');
		});
		if ($(this).val() !== '') {
			$.post($(this).attr('data-test-url'),
					{ pwd: $(this).val() },
					function(data){
						$('.password-strength').text(data.strength).addClass(clss[data.strength]);
					}, 'json');
		} else {
			$('.password-strength').text('Empty').addClass(clss['Empty']);
		}
	});
}

function profileSetup(responseText, textStatus, xhr){
	$("form#profile-form").ajaxForm({
		type:'PUT',
		target:'#content',
		beforeSubmit: function(){
			$('.submit-profile').button('loading');
		},
		success: profileSetup
	});
	setupPasswordTest();
}

function slideIn(container, content, setupFn){
	$(container).hide('slide', function() {
		$(this).empty().html(content).show('slide', {direction: 'right'}, setupFn);
	});
}

function loadSliding(url, setupFn){
	$.ajax({
		url:url, dataType:"html",
		success: function(html) { slideIn("#content", html, setupFn); }
	});
}

function homePageMenu(){
	$('.menu li').click(function(e) {
		$('.menu li').removeClass('active');
		var $this = $(this);
		$this.addClass('active');
		e.preventDefault();
	});
	$("a#components").click(function(evt){
		evt.preventDefault();
        loadComponents();
	});
    $("a#costing").click(function(evt){
   		evt.preventDefault();
        loadCostingStep1Screen();
   	});
	$("a#home").click(function(evt){
		evt.preventDefault();
		loadProducts();
	});
	$("a#profile").click(function(evt){
		evt.preventDefault();
		$('.menu li').removeClass('active');
		loadSliding($(this).attr('href'), profileSetup);
	});
}

function commonAjaxSetup() {
	$.ajaxSetup({ cache: false, timeout:60000 });
	$(document).ajaxError(function(event, xhr, settings, exc){
		handleXHRError(xhr);
	}).ajaxStart(function(){
        $("div.loading_modal").show();
    }).ajaxStop(function(){
        $("div.loading_modal").hide();
    }).ajaxComplete(function(){
    	$.placeholder();
    });
	$(".alert").alert();
}

function handleXHRError(xhr){
	if (xhr.status===0){
		alert("Oops! Looks like the server is not responding, please try again later.");
		return false;
	} else if (xhr.status==401){
		$("div.loading_modal").hide();
		window.location = '/';
		return false;
	} else if (xhr.status==500) {
		alert(xhr.responseText);
		return false;
	}
	return true;
}

// Home Page Loading...
function homePageSetup() {
	commonAjaxSetup();
	homePageMenu();
	loadProducts();
}
