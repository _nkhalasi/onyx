from flask import current_app, session
from wtforms.ext.csrf.session import SessionSecureForm
from datetime import timedelta


class _BaseForm(SessionSecureForm):
    SECRET_KEY = 'EPj00jpfj8Gx1SjnyLxwBBSQfnQ9DJYe0Ym'.encode('utf-8')
    TIME_LIMIT = timedelta(minutes=10)

    def __init__(self, formdata=None, obj=None, prefix='', csrf_enabled=None, csrf_context=None, **kwargs):
        if csrf_enabled is None:
            csrf_enabled = current_app.config.get('CSRF_ENABLED', True)
        self.csrf_enabled = csrf_enabled

        if self.csrf_enabled:
            if csrf_context is None:
                csrf_context = session
        else:
            csrf_context = {}
        #super().__init__(formdata=formdata, obj=obj, prefix=prefix, csrf_context=csrf_context, **kwargs)
        super(_BaseForm, self).__init__(formdata=formdata, obj=obj, prefix=prefix, csrf_context=csrf_context, **kwargs)

    def generate_csrf_token(self, csrf_context=None):
        if not self.csrf_enabled:
            return None
        #return super().generate_csrf_token(csrf_context)
        return super(_BaseForm, self).generate_csrf_token(csrf_context)

    def validate_csrf_token(self, field):
        if not self.csrf_enabled:
            return True
        #return super().validate_csrf_token(field)
        return super(_BaseForm, self).validate_csrf_token(field)
