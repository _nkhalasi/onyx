from onyx.forms import _BaseForm
from wtforms import TextField, PasswordField, BooleanField
from wtforms.validators import Required


class LoginForm(_BaseForm):
    username = TextField('Usernane', validators=(Required(),))
    password = PasswordField('Password', validators=(Required(),))
    remember_me = BooleanField('remember_me', default=False)