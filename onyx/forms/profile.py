from onyx.forms import _BaseForm
from wtforms import TextField, PasswordField
from wtforms.validators import Required, EqualTo, Email, Optional, ValidationError


class ProfileForm(_BaseForm):
    email = TextField(label='Email', validators=(Required(), Email(),))
    current_password = PasswordField(label='Current Password', validators=(Required(),))
    new_password = PasswordField(label='New Password',
                                 validators=(Optional(),
                                             EqualTo('re_password',
                                                     message='New Password must be equal to Confirm Password')))
    re_password = PasswordField(label='Confirm Password')