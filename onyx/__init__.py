from flask import Flask
import random
import string

app = Flask(__name__)


def secret_key():
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(32))


def initialize_flask_app(config_file):
    from onyx.utils import load_config, enable_logging, database_config, uploads_config
    from onyx.lib import any_component_price_depends_on_moc

    load_config(config_file)
    enable_logging(config_file)

    global app
    #app = Flask(__name__)
    app.config.update(
        SECRET_KEY='AkOXWp1Gg8OKnzSCze2OXT7kFTW9plEuo9p9wIbXIYLnWBfIwPGN7Qwe1Zr6Ck1w',
        DATABASE_URI=database_config()['url'],
        UPLOAD_FOLDER=uploads_config()['folder']
    )
    app.jinja_env.filters['any_component_price_depends_on_moc'] = any_component_price_depends_on_moc

    from sqlalchemy.engine import create_engine
    from onyx.models import get_db_session
    get_db_session(create_engine(app.config['DATABASE_URI']))

    # from onyx.utils.uploads import UploadFileWorker
    # UploadFileWorker().start()

    return app


#app = initialize_flask_app('/home/nkhalasi/myprojects/onyx/development.ini')

from onyx.views import login
from onyx.views import index
from onyx.views import profile
from onyx.views import products
