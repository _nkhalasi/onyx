if __name__ == '__main__':
    from onyx import initialize_flask_app
    initialize_flask_app('development.ini')

    from onyx import app
    app.run(host='0.0.0.0', debug=True)
