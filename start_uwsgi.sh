#! /bin/bash
cd $1
source pyenv-onyx/bin/activate
uwsgi --socket 127.0.0.1:3031 -w onyx:app --master --processes 4 --threads 2 --stats 127.0.0.1:9191 --pidfile /tmp/onyx-uwsgi.pid
#uwsgi --http 127.0.0.1:3031 -w onyx:app --master --processes 4 --threads 2 --stats 127.0.0.1:9191 --pidfile /tmp/onyx-uwsgi.pid
