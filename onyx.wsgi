ONYX_HOME = '/home/nkhalasi/myprojects/onyx'
APP_CONFIG = '{0}/development.ini'.format(ONYX_HOME)

activate_this = '{0}/py27/bin/activate_this.py'.format(ONYX_HOME)
execfile(activate_this, dict(__file__=activate_this))

from onyx import initialize_flask_app
initialize_flask_app(APP_CONFIG)

from onyx import app as application
