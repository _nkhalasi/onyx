from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

from onyx.utils import load_config, enable_logging, database_config
from sqlalchemy.engine import create_engine
from onyx.models import get_db_session

load_config('development.ini')
enable_logging('development.ini')
db_url = database_config()['url']
get_db_session(create_engine(db_url))

from onyx import app
app.config['DATABASE_URI'] = db_url

http_server = HTTPServer(WSGIContainer(app))
http_server.listen(8080)
IOLoop.instance().start()
