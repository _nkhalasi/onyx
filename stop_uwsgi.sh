#! /bin/bash
cd $1
source pyenv-onyx/bin/activate
uwsgi --stop /tmp/onyx-uwsgi.pid
